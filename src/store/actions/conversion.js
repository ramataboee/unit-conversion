import * as actionTypes from "./actionTypes";
//import convert from "convert-units";

export const conversionStart = () => {
    return {
        type: actionTypes.CONVERSION_START
    };
};

export const conversionSuccess = (data, from, to) => {
    return {
        type: actionTypes.CONVERSION_SUCCESS,
        data: data,
        from: from,
        to: to
    };
};

export const getConversionUnits = data => {
    return {
        type: actionTypes.GET_CONVERSION_UNITS,
        data: data
    };
};

export const conversionInvalid = data => {
    return {
        type: actionTypes.CONVRESION_INVALID,
        data: data
    };
};

export const getConversion = conversionData => {
    return (dispatch, getState) => {
        dispatch(conversionStart());

        dispatch(getConversionUnits(conversionData));

        if (getState().conversion.from && getState().conversion.to) {
            dispatch(
                conversionSuccess(
                    conversionData,
                    getState().conversion.from,
                    getState().conversion.to
                )
            );
        } else {
            dispatch(conversionInvalid(conversionData));
        }
    };
};
