import conversion from "./conversion";
import * as actionTypes from "../actions/actionTypes";

describe("conversion reducer", () => {
    it("should return initial state", () => {
        expect(conversion(undefined, {})).toEqual({
            output: null,
            from: null,
            to: null,
            result: []
        });
    });

    it("should reset units and output", () => {
        const initialState = {
            output: null,
            from: null,
            to: null,
            result: [
                {
                    inputValue: "84.6",
                    unitOfMeasure: "cuppps",
                    targetUnitOfMeasure: "Fahrenheit",
                    response: "9",
                    output: "invalid"
                }
            ]
        };

        const action = {
            type: actionTypes.CONVERSION_START
        };

        const expectedState = {
            output: null,
            from: null,
            to: null,
            result: [
                {
                    inputValue: "84.6",
                    unitOfMeasure: "cuppps",
                    targetUnitOfMeasure: "Fahrenheit",
                    response: "9",
                    output: "invalid"
                }
            ]
        };

        expect(conversion(initialState, action)).toEqual(expectedState);
    });

    it("should return conversion units", () => {
        const state = { output: null, from: null, to: null, result: [] };
        const action = {
            type: actionTypes.GET_CONVERSION_UNITS,
            data: {
                inputValue: "12",
                unitOfMeasure: "Fahrenheit",
                targetUnitOfMeasure: "Celsius",
                response: "5"
            }
        };

        const expectedOutput = { output: null, from: "F", to: "C", result: [] };

        expect(conversion(state, action)).toEqual(expectedOutput);
    });

    it("should convert units and return correctness of authoritative answer vs user input", () => {
        const state = { output: null, from: "F", to: "R", result: [] };

        const action = {
            type: "CONVERSION_SUCCESS",
            data: {
                inputValue: "84.2",
                unitOfMeasure: "Fahrenheit",
                targetUnitOfMeasure: "Rankine",
                response: "543.94"
            },
            from: "F",
            to: "R"
        };

        const expectedState = {
            output: "correct",
            from: "F",
            to: "R",
            result: [
                {
                    inputValue: "84.2",
                    unitOfMeasure: "Fahrenheit",
                    targetUnitOfMeasure: "Rankine",
                    response: "543.94",
                    output: "correct"
                }
            ]
        };

        expect(conversion(state, action)).toEqual(expectedState);
    });
});
