import * as actionTypes from "../actions/actionTypes";
import convert from "convert-units";
import * as R from "ramda";
import * as measures from "../../definitions/index";

const initialState = {
    output: null,
    from: null,
    to: null,
    result: []
};

/**
 * Resets validations and outputs
 */
const conversionStart = (state, action) => {
    return {
        ...state,
        output: null,
        from: null,
        to: null
    };
};

/**
 * Gets units from a valid domain of accepted units, return state
 */
const getConversionUnits = (state, action) => {
    //convert user units of measure to semi-sentense case
    const unitOfMeasure =
        action.data.unitOfMeasure.charAt(0).toUpperCase() +
        action.data.unitOfMeasure.slice(1).toLowerCase();

    const targetUnitOfMeasure =
        action.data.targetUnitOfMeasure.charAt(0).toUpperCase() +
        action.data.targetUnitOfMeasure.slice(1).toLowerCase();

    //use predefined scope
    const scope = [...measures.temperatures, ...measures.volumes];

    //Get units of measure
    const convertFrom =
        R.filter(R.where({ plural: R.equals(unitOfMeasure) }))(scope) ||
        R.filter(R.where({ singular: R.equals(unitOfMeasure) }))(scope);

    const from = convertFrom && convertFrom.length ? convertFrom[0].abbr : null;

    const convertTo =
        R.filter(R.where({ plural: R.equals(targetUnitOfMeasure) }))(scope) ||
        R.filter(R.where({ singular: R.equals(targetUnitOfMeasure) }))(scope);

    const to = convertTo && convertTo.length ? convertTo[0].abbr : null;

    //update state
    return {
        ...state,
        from: from,
        to: to
    };
};

/**
 * Converts units and returns output
 */
const conversionSuccess = (state, action) => {
    try {
        //convert units
        let answer = (
            Math.round(
                convert(parseFloat(action.data.inputValue))
                    .from(action.from)
                    .to(action.to) * 10
            ) / 10
        ).toFixed(2);

        //ensure user respose is rounded to the nearest tenth
        const response = (
            Math.round(parseFloat(action.data.response).toFixed(2) * 10) / 10
        ).toFixed(2);

        //authoritative answer
        const output = answer === response ? "correct" : "incorrect";

        return {
            ...state,
            output: output,
            result: [...state.result, { ...action.data, output: output }]
        };
    } catch {
        //impossible conversion of different measure,
        return {
            ...state,
            output: "invalid",
            result: [...state.result, { ...action.data, output: "invalid" }]
        };
    }
};

const conversionInvalid = (state, action) => {
    return {
        ...state,
        output: "invalid",
        result: [...state.result, { ...action.data, output: "invalid" }]
    };
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CONVERSION_START:
            return conversionStart(state, action);
        case actionTypes.GET_CONVERSION_UNITS:
            return getConversionUnits(state, action);
        case actionTypes.CONVERSION_SUCCESS:
            return conversionSuccess(state, action);
        case actionTypes.CONVRESION_INVALID:
            return conversionInvalid(state, action);
        default:
            return state;
    }
};

export default reducer;
