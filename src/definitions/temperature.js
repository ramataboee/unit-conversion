export const temperatures = [
    {
        abbr: "C",
        measure: "temperature",
        system: "metric",
        singular: "Celsius",
        plural: "Celsius"
    },
    {
        abbr: "K",
        measure: "temperature",
        system: "metric",
        singular: "Kelvin",
        plural: "Kelvin"
    },
    {
        abbr: "F",
        measure: "temperature",
        system: "imperial",
        singular: "Fahrenheit",
        plural: "Fahrenheit"
    },
    {
        abbr: "R",
        measure: "temperature",
        system: "imperial",
        singular: "Rankine",
        plural: "Rankine"
    }
];
