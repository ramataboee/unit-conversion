export const volumes = [
    {
        abbr: "mm3",
        measure: "volume",
        system: "metric",
        singular: "Cubic-millimeter",
        plural: "Cubic-millimeters"
    },
    {
        abbr: "cm3",
        measure: "volume",
        system: "metric",
        singular: "Cubic-centimeter",
        plural: "Cubic-centimeters"
    },
    {
        abbr: "ml",
        measure: "volume",
        system: "metric",
        singular: "Millilitre",
        plural: "Millilitres"
    },
    {
        abbr: "cl",
        measure: "volume",
        system: "metric",
        singular: "Centilitre",
        plural: "Centilitres"
    },
    {
        abbr: "dl",
        measure: "volume",
        system: "metric",
        singular: "Decilitre",
        plural: "Decilitres"
    },
    {
        abbr: "l",
        measure: "volume",
        system: "metric",
        singular: "Liter",
        plural: "Liters"
    },
    {
        abbr: "kl",
        measure: "volume",
        system: "metric",
        singular: "Kilolitre",
        plural: "Kilolitres"
    },
    {
        abbr: "m3",
        measure: "volume",
        system: "metric",
        singular: "Cubic-meter",
        plural: "Cubic-meters"
    },
    {
        abbr: "km3",
        measure: "volume",
        system: "metric",
        singular: "Cubic-kilometer",
        plural: "Cubic-kilometers"
    },
    {
        abbr: "krm",
        measure: "volume",
        system: "metric",
        singular: "Matsked",
        plural: "Matskedar"
    },
    {
        abbr: "tsk",
        measure: "volume",
        system: "metric",
        singular: "Tesked",
        plural: "Teskedar"
    },
    {
        abbr: "msk",
        measure: "volume",
        system: "metric",
        singular: "Matsked",
        plural: "Matskedar"
    },
    {
        abbr: "kkp",
        measure: "volume",
        system: "metric",
        singular: "Kaffekopp",
        plural: "Kaffekoppar"
    },
    {
        abbr: "glas",
        measure: "volume",
        system: "metric",
        singular: "Glas",
        plural: "Glas"
    },
    {
        abbr: "kanna",
        measure: "volume",
        system: "metric",
        singular: "Kanna",
        plural: "Kannor"
    },
    {
        abbr: "tsp",
        measure: "volume",
        system: "imperial",
        singular: "Teaspoon",
        plural: "Teaspoons"
    },
    {
        abbr: "Tbs",
        measure: "volume",
        system: "imperial",
        singular: "Tablespoon",
        plural: "Tablespoons"
    },
    {
        abbr: "in3",
        measure: "volume",
        system: "imperial",
        singular: "Cubic inch",
        plural: "Cubic-inches"
    },
    {
        abbr: "fl-oz",
        measure: "volume",
        system: "imperial",
        singular: "Fluid-ounce",
        plural: "Fluid-ounces"
    },
    {
        abbr: "cup",
        measure: "volume",
        system: "imperial",
        singular: "Cup",
        plural: "Cups"
    },
    {
        abbr: "pnt",
        measure: "volume",
        system: "imperial",
        singular: "Pint",
        plural: "Pints"
    },
    {
        abbr: "qt",
        measure: "volume",
        system: "imperial",
        singular: "Quart",
        plural: "Quarts"
    },
    {
        abbr: "gal",
        measure: "volume",
        system: "imperial",
        singular: "Gallon",
        plural: "Gallons"
    },
    {
        abbr: "ft3",
        measure: "volume",
        system: "imperial",
        singular: "Cubic-foot",
        plural: "Cubic-feet"
    },
    {
        abbr: "yd3",
        measure: "volume",
        system: "imperial",
        singular: "Cubic-yard",
        plural: "Cubic-yards"
    }
];
