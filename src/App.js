import React, { Component } from "react";
import Worksheet from "./components/Worksheet/Worksheet";

import { connect } from "react-redux";

import "./App.css";

import ConversionForm from "./containers/ConversionForm/ConversionForm";

class App extends Component {
    render() {
        return (
            <div>
                <ConversionForm />
                <Worksheet rows={this.props.result} />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        result: state.conversion.result
    };
};
export default connect(mapStateToProps)(App);
