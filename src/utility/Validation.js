export const validation = (value, rules) => {
    let isValid = true;

    if (rules.required) {
        isValid = value.trim() !== "" && isValid;
    }

    if (rules.isNumeric) {
        const pattern = /^\d*\.?\d*$/;
        isValid = pattern.test(value) && isValid;
    }

    return isValid;
};
