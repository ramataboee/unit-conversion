import React from "react";
import { shallow } from "enzyme";

import { ConversionForm } from "./ConversionForm";
import Input from "../../components/Input/Input";

describe("ConversionForm", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<ConversionForm />);
    });

    it("should have 4 inputs", () => {
        expect(wrapper.find(Input)).toHaveLength(4);
    });
});
