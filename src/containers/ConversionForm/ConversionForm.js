import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";
import Input from "../../components/Input/Input";
import Button from "../../components/Button/Button";
import { validation } from "../../utility/Validation";

import PropTypes from "prop-types";

import styles from "./ConversionForm.module.css";

export class ConversionForm extends Component {
    state = {
        conversionForm: {
            inputValue: {
                elementType: "input",
                elementConfig: {
                    type: "text"
                },
                value: "",
                label: "Input Numerical Value",
                name: "inputValue",
                validation: {
                    required: true,
                    isNumeric: true
                },
                valid: false,
                touched: false
            },
            unitOfMeasure: {
                elementType: "input",
                elementConfig: {
                    type: "text"
                },
                value: "",
                label: "Input Unit of Measure",
                name: "unitOfMeasure",
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            targetUnitOfMeasure: {
                elementType: "input",
                elementConfig: {
                    type: "text"
                },
                value: "",
                label: "Target Unit Of Measure",
                name: "targetUnitOfMeasure",
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            response: {
                elementType: "input",
                elementConfig: {
                    type: "text"
                },
                value: "",
                label: "Student Response",
                name: "response",
                validation: {
                    isNumeric: true
                },
                valid: true,
                touched: false
            }
        },
        formIsValid: false
    };

    inputChangeHandler = (event, identifier) => {
        const updatedForm = {
            ...this.state.conversionForm
        };
        const updatedElement = {
            ...updatedForm[identifier]
        };

        updatedElement.value = event.target.value;
        updatedElement.valid = validation(
            updatedElement.value,
            updatedElement.validation
        );
        updatedElement.touched = true;
        updatedForm[identifier] = updatedElement;

        //check if the entire form is valid
        let formIsValid = true;
        for (let input in updatedForm) {
            formIsValid = updatedForm[input].valid && formIsValid;
        }

        this.setState({
            conversionForm: updatedForm,
            formIsValid: formIsValid
        });
    };

    conversionHandler = event => {
        event.preventDefault();

        const inputValues = [];

        for (let index in this.state.conversionForm) {
            inputValues[index] = this.state.conversionForm[index].value;
        }

        const conversionData = {
            ...inputValues
        };

        this.state.formIsValid
            ? this.props.onConvert(conversionData)
            : this.props.onIsNotNumeric(conversionData);
    };

    render() {
        let inputsForm = [];

        for (let i in this.state.conversionForm) {
            inputsForm.push({
                id: i,
                config: this.state.conversionForm[i]
            });
        }

        let form = (
            <form onSubmit={this.conversionHandler}>
                {inputsForm.map(element => {
                    return (
                        <Input
                            key={element.id}
                            elementType={element.config.elementType}
                            elementConfig={element.config.elementConfig}
                            value={element.config.value}
                            name={element.config.name}
                            label={element.config.label}
                            changed={event =>
                                this.inputChangeHandler(event, element.id)
                            }
                            invalid={!element.config.valid}
                            touched={element.config.touched}
                        />
                    );
                })}
                <Button type="primary" clicked={() => {}}>
                    Convert
                </Button>
            </form>
        );

        return <div className={styles.formContainer}>{form}</div>;
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onConvert: convertData => dispatch(actions.getConversion(convertData)),
        onIsNotNumeric: convertData =>
            dispatch(actions.conversionInvalid(convertData))
    };
};

ConversionForm.propTypes = {
    onConvert: PropTypes.func,
    onIsNotNumeric: PropTypes.func
};

export default connect(null, mapDispatchToProps)(ConversionForm);
