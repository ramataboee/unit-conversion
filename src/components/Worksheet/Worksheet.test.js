import React from "react";
import { shallow } from "enzyme";
import Worksheet from "./Worksheet";

describe("Worksheet", () => {
    it("should render rows", () => {
        const result = [
            {
                inputValue: "84.6",
                unitOfMeasure: "cuppps",
                targetUnitOfMeasure: "Fahrenheit",
                response: "9",
                output: "invalid"
            }
        ];
        const wrapper = shallow(<Worksheet rows={result} />);
        expect(wrapper.find("tbody").children()).toBeDefined();
        expect(wrapper.find("tbody").children()).toHaveLength(result.length);
    });
});
