import React from "react";
import styles from "./Worksheet.module.css";
import PropTypes from "prop-types";

//import Cell from "./Cell/Cell";

const worksheet = props => {
    const { rows } = props;
    let tableMarkup = null;

    if (!rows || rows.length === 0) {
        tableMarkup = (
            <p className={styles.noResults}>
                No Conversions have been made yet
            </p>
        );
    } else {
        const rowsMarkup = rows.map((row, i) => {
            return (
                <tr key={`row-${i}`}>
                    <td>{row.inputValue}</td>
                    <td>{row.unitOfMeasure}</td>
                    <td>{row.targetUnitOfMeasure}</td>
                    <td>{row.response}</td>
                    <td>{row.output}</td>
                </tr>
            );
        });

        tableMarkup = (
            <table className={styles.worksheet}>
                <thead></thead>
                <tbody>{rowsMarkup}</tbody>
            </table>
        );
    }

    return <>{tableMarkup}</>;
};

worksheet.propTypes = {
    rows: PropTypes.array
};

export default worksheet;
