import React from "react";
import styles from "./Input.module.css";
import PropTypes from "prop-types";

const input = props => {
    let inputElement = null;
    const style = [styles.inputElement];

    if (props.invalid && props.touched) style.push(styles.invalid);

    switch (props.elementType) {
        case "input":
            inputElement = (
                <input
                    className={style.join(" ")}
                    {...props.elementConfig}
                    name={props.name}
                    value={props.value}
                    onChange={props.changed}
                />
            );
            break;
        case "select":
            inputElement = (
                <select
                    onChange={props.changed}
                    name={props.name}
                    value={props.value}>
                    {props.elementConfig.options.map(option => (
                        <option key={option.value} value={option.value}>
                            {option.display}
                        </option>
                    ))}
                </select>
            );
            break;

        default:
            inputElement = <input {...props.elementConfig} />;
    }

    return (
        <div className={styles.inputContainer}>
            <label>{props.label}</label>
            {inputElement}
        </div>
    );
};

input.propTypes = {
    elementType: PropTypes.oneOf(["select", "input", "textarea", "button"]),
    elementConfig: PropTypes.object,
    changed: PropTypes.func,
    name: PropTypes.string,
    value: PropTypes.string,
    label: PropTypes.string
};

export default input;
